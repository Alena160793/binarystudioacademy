//Task1
let x = 5
let y = 3
let result
if(x < y){
     result = x + y
 }else if(x > y){
     result = x - y
 }else {
    result = x * y 
 }
 console.log(result)

 //Task2
 function getArraySum(arr1, arr2){
    let s = 0
    for (let v = 0; v < arr1.length; v++){
       s = s + arr1[v]
   }
   let e = 0
    for (let p = 0; p < arr2.length; p++){
       e = e + arr2[p]
   }
    return (s+e)
}

let r = getArraySum([2,3], [4,5])
console.log(r)

//Task3
function getCountTrue(arr) {
    let resultq = 0
    for (let i=0; i<arr.length; i++){
        if(arr[i] == true) {
        resultq++
        } else {
           resultq = resultq + 0
        }
    }
    return resultq
}

let d = getCountTrue([true, false, true])
console.log(d)

//Task4
function doubleFactorial(number){
    let k = 1
    if (number == 0){
        k = 1
    } else{
        while(number>0) {
            k = k*number
            number = number-2
        }
    }
    return k
}

let z = doubleFactorial(9)
console.log(z)

//Task5
const person = [
    {name: 'Lisa', age: 18},
    {name: 'Mira', age: 25},
    {name: 'Sasha', age: 30},
    {name: 'Dima', age: 26}
]

person.sort(mySort1)

function mySort1(a, b) {
    if(a.age > b.age) {
        return 1 
    }else if(a.age < b.age){
        return -1
    }else {
    return 0
    }
}

console.log(person)

person.sort(mySort2)

function mySort2(u, t) {
    if(u.age < t.age)
        return 1 
    if(u.age > t.age)
        return -1
    return 0
}

console.log(person)

let person12 = {name: 'Lisa', age: 40}
let person22 = {name: 'Misha', age: 20}

function compareAge(person1, person2) {
    let f
if (person1.age === person2.age) {
     f = 'is the same age as'
}else if(person1.age > person2.age) {
     f ='is older'
}else{
     f ='is younger'
}
return f
}
console.log(person12.name)
let o = compareAge(person12, person22)
console.log(o)
console.log(person22.name)